from flask import Flask, render_template, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import jwt
import datetime
from functools import wraps

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1djubijb10@localhost/PythonCRUD'
app.config['SECRET_KEY']='secretkey'
db = SQLAlchemy(app)
CORS(app)

def token_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        token = None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'mensaje':'Falta el token!'}),403

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return jsonify({'mensaje':'Token invalido'}),403
        
        return f(*args,**kwargs)
    return decorated

class Persona(db.Model):
    __tablename__ = "Personas"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(255))
    fechanacimiento = db.Column(db.String(255))
    puesto = db.Column(db.String(255))
    
    def __init__(self,id, nombre, fechanacimiento,puesto):
        self.id= id
        self.nombre = nombre
        self.fechanacimiento = fechanacimiento
        self.puesto = puesto
    
   # def __repr__(self):
   #     return '%s/%s/%s' % (self.id, self.nombre, self.fechanacimiento, self.puesto)

@app.route('/')
def index():
    auth= request.authorization
    
    if auth and auth.username == 'patsy' and auth.password =='password':
        token = jwt.encode({'user':auth.username, 'exp': datetime.datetime.utcnow()+ datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])
        return jsonify({'token' : token.decode('UTF-8')})
        #return render_template('home.html',tokenstr = token)
    
    return make_response('No se pudo verificar!',401,{'WWW-Authenticate':'Basic realm="Login Requerido"'})


@app.route('/postdata', methods=['POST','GET'])
@token_required
def postdata():
    # POST
    if request.method == 'POST':
        body = request.get_json()
        nombre = body['nombre']
        fechanacimiento = body['fechanacimiento']
        puesto = body['puesto']

        data = Persona(nombre, fechanacimiento, puesto)
        db.session.add(data)
        db.session.commit()

        return jsonify({
            'estatus': 'La informacion ha sido insertada en PostgreSQL',
            'nombre': nombre,
            'fechanacimiento': fechanacimiento,
            'puesto':puesto
        })
    
    # GET todos
    if request.method == 'GET':
        data = Persona.query.order_by(Persona.id).all()
        print(data)
        if not data:
            return jsonify({'mensaje':'No hay personas para mostrar'})
        dataJson = []
        for i in data:
            dataDict = {}
            dataDict['id'] = i.id
            dataDict['nombre'] = i.nombre
            dataDict['fechanacimiento']= i.fechanacimiento
            dataDict['puesto'] = i.puesto
            dataJson.append(dataDict)
        return jsonify(dataJson)

@app.route('/managedata/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def managedata(id):

    # GET por id
    if request.method == 'GET':
        data = Persona.query.get(id)
        print(data)
        if not data:
            return jsonify({'mensaje':'La persona no fue encontrada'})
        dataDict = {}
        dataDict['id'] = data.id
        dataDict['nombre'] = data.nombre
        dataDict['fechanacimiento']= data.fechanacimiento
        dataDict['puesto'] = data.puesto
        return jsonify(dataDict)
        
    # DELETE
    if request.method == 'DELETE':
        delData = Persona.query.filter_by(id=id).first()
        db.session.delete(delData)
        db.session.commit()
        return jsonify({'estatus': 'La persona '+id+' ha sido eliminada de PostgreSQL'})

    # UPDATE por id
    if request.method == 'PUT':
        body = request.get_json()
        print(body)
        nombre = body['nombre']
        fechanacimiento = body['fechanacimiento']
        puesto = body['puesto']
        editData = Persona.query.filter_by(id=id).first()
        editData.nombre = nombre
        editData.fechanacimiento = fechanacimiento
        editData.puesto = puesto
        db.session.commit()
        return jsonify({'status': 'La persona '+id+' ha sido actualizada en PostgreSQL'})

if __name__ == '__main__':
    app.debug = True
    app.run()